package ShowMySQlUI;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class NewField extends JDialog {
	private String FieldName;
	private String FieldType;
	private String FieldConstraint;
	private UI main;
	JDialog jd = new JDialog(this, "字段设置");
	private JComboBox<String> jb = new JComboBox<String>();
	private JComboBox<String> jb2 = new JComboBox<String>();
	JComboBox<String> jb3 = new JComboBox<String>();
	private JTextField FieldNamed = new JTextField(21);

	public void FB() {
		jd.setSize(1050, 200);
		jd.setLocation(500, 500);
		jd.setLayout(new FlowLayout());
		jb2.setVisible(false);
		jb.addItem("请选择字段类型");
		jb.addItem("整型");
		jb.addItem("浮点");
//		jb.addItem("定点");
		jb.addItem("串型");
		jb.addItem("日期");
		jb.addItem("二进制");
		jb2.addItem("---------");
		jb.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String s = (String) jb.getSelectedItem();
				switch (s) {
				case "整型":
					jb2.setVisible(true);
					jb2.removeAllItems();
					jb2.addItem("byte");
					jb2.addItem("smallint");
					jb2.addItem("mediumint");
					jb2.addItem("int");
					jb2.addItem("bigint");
					break;
				case "浮点":
					jb2.setVisible(true);
					jb2.removeAllItems();
					jb2.addItem("float");
					jb2.addItem("double");
					break;
//				case "定点":// decimal(n,m) m小数点后位数
				case "串型":
					jb2.setVisible(true);
					jb2.removeAllItems();
					jb2.addItem("char");
					jb2.addItem("varchar(100)");
					break;
				case "日期":
					jb2.setVisible(true);
					jb2.removeAllItems();
					jb2.addItem("date");
					jb2.addItem("time");
					jb2.addItem("datetime");
					break;
				case "二进制":
					jb2.setVisible(true);
					jb2.removeAllItems();
					jb2.addItem("binary");
					jb2.addItem("varbinary");
					break;
				default:
					break;
				}
			}
		});
		jb3.addItem("---不约束---");
		jb3.addItem("primary key");// 主建约束，同时保证唯一性和非空
		jb3.addItem("primary key auto_increment");// 主建约束,且数据自增
		jb3.addItem("NOT NULL");// 非空约束
		jb3.addItem("unique");// 唯一约束，但可以为空
		JLabel tips2 = new JLabel("输入字段名");
		JButton addzd = new JButton("确认添加");
		addzd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				jd.setVisible(false);
				jd.dispose();
			}
		});
		jd.add(tips2);
		jd.add(FieldNamed);
		jd.add(jb);
		jd.add(jb2);
		jd.add(jb3);
		jd.add(addzd);
		jd.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		jd.setModal(true);
		jd.setVisible(true);
	}

	public String getFieldName() {
		FieldName = FieldNamed.getText();
		return FieldName;
	}

	public String getFieldType() {
		FieldType = jb2.getSelectedItem().toString();
		return FieldType;
	}

	public String getFieldConstraint() {
		FieldConstraint = jb3.getSelectedItem().toString();
		return FieldConstraint;
	}
}
