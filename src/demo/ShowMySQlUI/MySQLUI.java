package ShowMySQlUI;

import java.awt.event.*;
import java.sql.*;

import javax.swing.*;
@SuppressWarnings("serial")
class UI extends JFrame {
	// 主窗口
	final JPanel panel = new JPanel();
	final JPanel PL = new JPanel();
	final JPanel PR = new JPanel();
	UI() {
		setTitle("MySQL");
		setSize(1200, 800);
		setLocation(500, 300);

		PL.setLayout(null);
		// 新建
		JPanel PLFound = new JPanel();
		PLFound.setBorder(BorderFactory.createTitledBorder("新建"));
		PLFound.setSize(200, 100);
		JButton foundk = new JButton("新建库");
		JButton foundB = new JButton("新建表");
		JButton AddData = new JButton("表中添加数据");
		PLFound.add(foundk);
		PLFound.add(foundB);
		PLFound.add(AddData);
		PL.add(PLFound);
		// 查看
		JPanel PLLook = new JPanel();
		PLLook.setLocation(0, 100);
		PLLook.setSize(200, 100);
		PLLook.setBorder(BorderFactory.createTitledBorder("查看"));
		JButton LookK = new JButton("查看库");
		JButton LookB = new JButton("查看表");
		PLLook.add(LookK);
		PLLook.add(LookB);
		PL.add(PLLook);
		// 查找指定数据
		JPanel Seek = new JPanel();
		Seek.setLocation(0, 200);
		Seek.setSize(200, 100);
		Seek.setBorder(BorderFactory.createTitledBorder("查找指定数据"));
		JButton SeekB = new JButton("查找");
		Seek.add(SeekB);
		PL.add(Seek);
		// 修改数据
		JPanel Alter = new JPanel();
		Alter.setLocation(0, 300);
		Alter.setSize(200, 200);
		Alter.setBorder(BorderFactory.createTitledBorder("修改数据"));
		JButton AlterB = new JButton(" 修  改  表  名 ");
		JButton AlterFieldName = new JButton("修改  字  段名");
		JButton AlterFieldType = new JButton("修改字段类型");
		JButton AddField = new JButton(" 添  加  字  段 ");
		Alter.add(AlterB);
		Alter.add(AlterFieldName);
		Alter.add(AlterFieldType);
		Alter.add(AddField);
		PL.add(Alter);
		// 删除
		JPanel Dle = new JPanel();
		Dle.setLocation(0, 500);
		Dle.setSize(200, 100);
		Dle.setBorder(BorderFactory.createTitledBorder("删除"));
		JButton DleK = new JButton("删除库");
		JButton DleB = new JButton("删除表");
		JButton DleField = new JButton("删除字段");
		Dle.add(DleK);
		Dle.add(DleB);
		Dle.add(DleField);
		PL.add(Dle);
		PL.setSize(200, 800);

		add(PL);
		add(PR);
		new initsql();
		Connection con = initsql.getcon();
		Fun fun = new Fun();
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				initsql.close(con);
				System.out.println("退出成功");
			}
		});
		foundk.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				fun.FK(con);
			}
		});
		foundB.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				fun.FB(con);
			}

		});
		AddData.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				fun.AD(con);
			}
		});
		LookK.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				fun.LK(con);
			}

		});
		LookB.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				fun.LB(con);
			}

		});
		SeekB.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				fun.SB(con);
			}

		});
		AlterB.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				fun.AB(con);
			}

		});
		AlterFieldName.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				fun.AFN(con);
			}

		});
		AlterFieldType.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				fun.AFT(con);
			}

		});
		AddField.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				fun.AF(con);
			}

		});
		DleK.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				fun.DK(con);
			}
		});
		DleB.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				fun.DB(con);
			}

		});
		DleField.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				fun.DF(con);
			}

		});
//		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setVisible(true);
	}
}
public class MySQLUI {
	public static void main(String[] args) {
		new UI();
	}
}