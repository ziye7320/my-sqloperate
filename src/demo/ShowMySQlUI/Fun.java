package ShowMySQlUI;

import java.awt.*;
import java.awt.event.*;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

import org.apache.log4j.Logger;

@SuppressWarnings("serial")
public class Fun extends JDialog {
	private static final Logger log = Logger.getLogger(UI.class.getSimpleName());

//新建库
	public void FK(Connection con) {
		JDialog jd = new JDialog(this, "新建数据库");
		jd.setSize(1050, 800);
		jd.setLocation(500, 500);
		jd.setLayout(new FlowLayout());
		JTextField FKname = new JTextField(20);
		JLabel tips = new JLabel("输入库名");
		JButton btn = new JButton("确认创建");
		jd.add(tips);
		jd.add(FKname);
		jd.add(btn);
		btn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Statement st = null;
				try {
					st = con.createStatement();
					String sql = " create database " + FKname.getText();
					int result = st.executeUpdate(sql);
					if (result == 1) {
						System.out.println("创建成功");
					} else {
						System.out.println("创建失败");
					}
				} catch (SQLException e1) {
					System.out.println("创建失败");
					log.debug(e1);
//					e1.printStackTrace();
				} finally {
					initsql.close(st);
				}
			}
		});
		jd.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		jd.setModal(true);
		jd.setVisible(true);
	}

//新建表
	public void FB(Connection con) {
		JDialog jd = new JDialog(this, "请选择要建表的库");
		jd.setSize(1050, 800);
		jd.setLocation(500, 500);
		jd.setLayout(new FlowLayout());
		JComboBox<String> jb = new JComboBox<String>();
		JButton btn = new JButton("确认");
		Statement st = null;
		ResultSet rs = null;
		try {
			st = con.createStatement();
			String sql = "show databases";
			rs = st.executeQuery(sql);
			ArrayList<String> list = new ArrayList<String>();
			jb.addItem("请选择要建表的库");
			while (rs.next()) {
				list.add(rs.getString(1));
			}
			for (String string : list) {
				jb.addItem(string);
			}
		} catch (SQLException e) {
			log.debug(e);
//			e.printStackTrace();
		} finally {
			initsql.close(st, rs);
		}
		btn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (jb.getSelectedItem().equals("请选择要建表的库")) {
					System.out.println("请选择要建表的库");
				} else {
					Statement st = null;
					try {
						st = con.createStatement();
						String sql = "use " + jb.getSelectedItem();
						st.executeUpdate(sql);
						JDialog jd1 = new JDialog();
						jd1.setTitle("创建表");
						jd1.setSize(1050, 800);
						jd1.setLocation(500, 500);
						jd1.setLayout(new FlowLayout());
						JButton setField = new JButton("创建");
						JButton addzd = new JButton("添加字段");
						JLabel tips1 = new JLabel("输入表名");
						JTextField Bname = new JTextField(55);
						jd1.add(tips1);
						jd1.add(Bname);
						jd1.add(addzd);
						jd1.add(setField);
						JPanel FBJP1 = new JPanel();
						List<NewField> list = new ArrayList<NewField>();
						addzd.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent e) {
								NewField n1 = new NewField();
								n1.FB();
								list.add(n1);
								setVisible(true);

							}
						});
						setField.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent e) {
								if (list.size() == 0) {
									System.out.println("请添加至少一个字段");
								} else {
									// 得到字段名，类型，约束
									List<String> list1 = new ArrayList<String>();
									List<String> list2 = new ArrayList<String>();
									List<String> list3 = new ArrayList<String>();
									for (NewField newField : list) {
										list1.add(newField.getFieldName());
										list2.add(newField.getFieldType());
										list3.add(newField.getFieldConstraint());
									}
									Statement st = null;
									StringBuffer Fieldnames = new StringBuffer();
									for (int i = 0; i < list1.size(); i++) {
										if (list3.get(i).equals("---不约束---")) {
											Fieldnames.append(list1.get(i) + " " + list2.get(i) + ",");
										} else {
											Fieldnames.append(
													list1.get(i) + " " + list2.get(i) + " " + list3.get(i) + ",");
										}
									}
									Fieldnames.deleteCharAt(Fieldnames.length() - 1);
									String BName = Bname.getText();
									String sql = "create table " + BName + "(" + Fieldnames + ")";
									try {
										st = con.createStatement();
										st.executeUpdate(sql);
										System.out.println("创建成功");
									} catch (SQLException e1) {
										log.debug(e1);
//										e1.printStackTrace();
									} finally {
										initsql.close(st);
									}
								}

							}
						});
						jd1.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
						jd1.setModal(true);
						jd1.setVisible(true);
					} catch (SQLException e2) {
						log.debug(e2);
//						e2.printStackTrace();
					} finally {
						initsql.close(st);
					}
				}
			}
		});
		jd.add(btn);
		jd.add(jb);
		jd.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		jd.setModal(true);
		jd.setVisible(true);
	}

//添加数据
	public void AD(Connection con) {
		JDialog jd = new JDialog(this, "添加字段数据");
		jd.setSize(1050, 800);
		jd.setLocation(500, 500);
		jd.setLayout(new FlowLayout());
		JComboBox<String> jb = new JComboBox<String>();
		JComboBox<String> jb2 = new JComboBox<String>();
		JButton btn = new JButton("确认");
		jb.addItem("请选择要操作的库");
		jb2.addItem("请选择需要添加数据的表");
		btn.setVisible(false);
		Statement st = null;
		ResultSet rs = null;
		try {
			st = con.createStatement();
			String sql = "show databases";
			rs = st.executeQuery(sql);
			ArrayList<String> list = new ArrayList<String>();
			int i = 0;
			while (rs.next()) {
				list.add(rs.getString(1));
				jb.addItem(list.get(i));
				i++;
			}
		} catch (SQLException e) {
			log.debug(e);
//			e.printStackTrace();
		} finally {
			initsql.close(st, rs);
		}
		jb.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (jb.getSelectedItem().equals("请选择要操作的库")) {
					System.out.println("请选择要操作的库");
				} else {
					Statement st = null;
					ResultSet rs = null;
					btn.setVisible(true);
					jb2.removeAllItems();
					try {
						st = con.createStatement();
						String sql = "use " + jb.getSelectedItem();
						st.executeUpdate(sql);
						String sql1 = "show tables";
						rs = st.executeQuery(sql1);
						jb2.addItem("请选择需要添加数据的表");
						JButton btn2 = new JButton("确认");
						while (rs.next()) {
							jb2.addItem(rs.getString(1));
						}
						btn.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent e) {
								if (jb2.getSelectedItem().equals("请选择需要添加数据的表")) {
									System.out.println("请选择需要添加数据的表");
								} else {
									Statement st = null;
									ResultSet rs = null;
									JDialog jd3 = new JDialog();
									jd3.setTitle("以下为可以添加字段的字段名");
									jd3.setSize(1050, 800);
									jd3.setLocation(500, 500);
									jd3.setLayout(new FlowLayout());

									try {
										st = con.createStatement();
										String sql2 = "select COLUMN_NAME, column_comment from INFORMATION_SCHEMA.Columns where table_name='"
												+ jb2.getSelectedItem() + "'";
										rs = st.executeQuery(sql2);
										ArrayList<String> list2 = new ArrayList<String>();
										// 得到所有字段名，放入list2
										while (rs.next()) {
											list2.add(rs.getString(1));
										}
										JButton addbtn = new JButton("添加");
										JLabel inputTips = new JLabel("请按字段顺序添加数据，请用英文逗号隔开，一次添加一条数据");
										JTextField addField = new JTextField(40);

										// 定义JTable的对象
										Vector rowData, columnNames;
										// 定义一行数据的对象
										Vector<String> line1;
										line1 = new Vector<String>();
										columnNames = new Vector<String>();
										rowData = new Vector();
										for (int i = 0; i < list2.size(); i++) {
											columnNames.add(list2.get(i));
										}
										String sql3 = "select*from " + jb2.getSelectedItem();// 得到所有字段的数据
										rs = st.executeQuery(sql3);
										// 所有字段的数据
										ArrayList<String> list3 = new ArrayList<String>();
										while (rs.next()) {
											for (int i = 1; i <= list2.size(); i++) {
												list3.add(rs.getString(i));
											}
										}
										int leng = 0;
										for (int j = 0; j < list3.size() / list2.size(); j++) {
											line1 = new Vector();
											for (int i = 0; i < list2.size(); i++) {
												line1.add(list3.get(leng++));
											}
											rowData.add(line1);
										}

										JTable jTable = new JTable(rowData, columnNames);
										jTable.setEnabled(false);
										addbtn.addActionListener(new ActionListener() {
											public void actionPerformed(ActionEvent e) {
												// 得到输入的结果
												String[] str = addField.getText().split(",");
												StringBuffer addFieldnames = new StringBuffer();
												StringBuffer addFieldnames1 = new StringBuffer();
												for (int i = 0; i < list2.size(); i++) {
													addFieldnames.append(list2.get(i) + ",");
													addFieldnames1.append("?,");
												}
												addFieldnames.deleteCharAt(addFieldnames.length() - 1);
												addFieldnames1.deleteCharAt(addFieldnames1.length() - 1);
												String sql4 = "insert into " + jb2.getSelectedItem() + " ("
														+ addFieldnames + ")values(" + addFieldnames1 + ")";

												PreparedStatement ps = null;
												try {
													ps = con.prepareStatement(sql4);
													for (int i = 1; i <= str.length; i++) {
														ps.setObject(i, str[i - 1]);
													}
													int result = ps.executeUpdate(); // 执行SQL获取受影响的行数
													System.out.println(result);
													if (result == 1) {
														System.out.println("添加成功");
													} else {
														System.out.println("添加失败");
													}
												} catch (SQLException e1) {
													log.debug(e1);
//														e1.printStackTrace();
												} finally {
													initsql.close(ps);
												}
											}
										});
										JScrollPane jScrollPane = new JScrollPane(jTable);
										jd3.add(jScrollPane);
										jd3.add(addField);
										jd3.add(addbtn);
										jd3.add(inputTips);
									} catch (SQLException e1) {
										log.debug(e1);
//										e1.printStackTrace();
									} finally {
										initsql.close(st, rs);
									}
									jd3.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
									jd3.setModal(true);
									jd3.setVisible(true);
								}
							}
						});
					} catch (SQLException e1) {
						log.debug(e1);
//						e1.printStackTrace();
					} finally {
						initsql.close(st, rs);
					}

				}
			}
		});
		jd.add(jb);
		jd.add(jb2);
		jd.add(btn);
		jd.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		jd.setModal(true);
		jd.setVisible(true);
	}

//查看库
	public void LK(Connection con) {
		JDialog jd = new JDialog(this, "所有库");
		jd.setSize(1050, 800);
		jd.setLocation(500, 500);
		jd.setLayout(new FlowLayout());
		// 定义组件
		JTable jTable = null;
		JScrollPane jScrollPane = null;
		// 定义JTable的对象
		Vector rowData, columnNames;
		// 定义一行数据的对象
		Vector<String> line1;
		line1 = new Vector<String>();
		Statement st = null;
		ResultSet rs = null;
		try {
			st = con.createStatement();
			String sql = "show databases";
			rs = st.executeQuery(sql);
			ArrayList<String> list = new ArrayList<String>();
			while (rs.next()) {
				list.add(rs.getString(1));
			}
			columnNames = new Vector<String>();
			columnNames.add("以下为所有可查询到的库");
			rowData = new Vector();
			for (int i = 0; i < list.size(); i++) {
				line1 = new Vector();
				line1.add(list.get(i));
				rowData.add(line1);
			}
			jTable = new JTable(rowData, columnNames);
			jTable.setEnabled(false);
			jScrollPane = new JScrollPane(jTable);
			jd.add(jScrollPane);
		} catch (SQLException e) {
			log.debug(e);
//			e.printStackTrace();
		} finally {
			initsql.close(st, rs);
		}
		jd.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		jd.setModal(true);
		jd.setVisible(true);
	}

//查看表
	public void LB(Connection con) {
		JDialog jd = new JDialog(this, "请选择查那个库的表");
		jd.setSize(1050, 800);
		jd.setLocation(500, 500);
		jd.setLayout(new FlowLayout());
		JComboBox<String> jb = new JComboBox<String>();
		JComboBox<String> jb2 = new JComboBox<String>();
		JButton btn = new JButton("确认");
		btn.setVisible(false);
		Statement st = null;
		ResultSet rs = null;
		try {
			st = con.createStatement();
			String sql = "show databases";
			rs = st.executeQuery(sql);
			ArrayList<String> list = new ArrayList<String>();
			jb.addItem("请选择查那个库的表");
			jb2.addItem("请选择要查的表");
			while (rs.next()) {
				list.add(rs.getString(1));
			}
			for (String string : list) {
				jb.addItem(string);
			}
		} catch (SQLException e) {
			log.debug(e);
//			e.printStackTrace();
		} finally {
			initsql.close(st, rs);
		}
		jb.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (jb.getSelectedItem().equals("请选择查那个库的表")) {
					System.out.println("请选择查那个库的表");
				} else {
					jb2.removeAllItems();
					Statement st = null;
					ResultSet rs = null;
					try {
						st = con.createStatement();
						String sql = "use " + jb.getSelectedItem();
						st.executeUpdate(sql);
						String sql1 = "show tables";
						rs = st.executeQuery(sql1);
						jb2.addItem("请选择要查看的表");
						while (rs.next()) {
							jb2.addItem(rs.getString(1));
						}

						jb2.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent e) {
								if (jb2.getSelectedItem().equals("请选择要查看的表")) {
									System.out.println("请选择要查看的表");
								} else {
									btn.setVisible(true);
									btn.addActionListener(new ActionListener() {
										public void actionPerformed(ActionEvent e) {
											Statement st = null;
											ResultSet rs = null;
											JDialog jd3 = new JDialog();
											jd3.setTitle("以下为该表所有数据");
											jd3.setSize(1050, 800);
											jd3.setLocation(500, 500);
											jd3.setLayout(new FlowLayout());
											try {
												st = con.createStatement();
												String sql2 = "select COLUMN_NAME, column_comment from INFORMATION_SCHEMA.Columns where table_name='"
														+ jb2.getSelectedItem() + "'";
												rs = st.executeQuery(sql2);
												ArrayList<String> list2 = new ArrayList<String>();
												// 得到所有字段名，放入list2
												while (rs.next()) {
													list2.add(rs.getString(1));
												}
												// 定义JTable的对象
												Vector rowData, columnNames;
												// 定义一行数据的对象
												Vector<String> line1;
												line1 = new Vector<String>();
												columnNames = new Vector<String>();
												rowData = new Vector();
												for (int i = 0; i < list2.size(); i++) {
													columnNames.add(list2.get(i));
												}
												String sql3 = "select*from " + jb2.getSelectedItem();// 得到所有字段的数据
												rs = st.executeQuery(sql3);
												// 所有字段的数据
												ArrayList<String> list3 = new ArrayList<String>();
												while (rs.next()) {
													for (int i = 1; i <= list2.size(); i++) {
														list3.add(rs.getString(i));
													}
												}
												int leng = 0;
												for (int j = 0; j < list3.size() / list2.size(); j++) {
													line1 = new Vector();
													for (int i = 0; i < list2.size(); i++) {
														line1.add(list3.get(leng++));
													}
													rowData.add(line1);
												}

												JTable jTable = new JTable(rowData, columnNames);
												jTable.setEnabled(false);
												JScrollPane jScrollPane = new JScrollPane(jTable);
												jd3.add(jScrollPane);
											} catch (SQLException e1) {
												log.debug(e1);
//												e1.printStackTrace();
											} finally {
												initsql.close(st, rs);
											}
											jd3.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
											jd3.setModal(true);
											jd3.setVisible(true);
										}
									});
								}
							}
						});
					} catch (SQLException e1) {
						log.debug(e1);
//						e1.printStackTrace();
					} finally {
						initsql.close(st, rs);
					}
				}
			}
		});
		jd.add(jb);
		jd.add(jb2);
		jd.add(btn);
		jd.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		jd.setModal(true);
		jd.setVisible(true);
	}

//查找指定数据
	public void SB(Connection con) {
		JDialog jd = new JDialog(this, "添加字段数据");
		jd.setSize(1050, 800);
		jd.setLocation(500, 500);
		jd.setLayout(new FlowLayout());
		JComboBox<String> jb = new JComboBox<String>();
		JComboBox<String> jb2 = new JComboBox<String>();
		JButton btn = new JButton("确认");
		jb.addItem("请选择要操作的库");
		jb2.addItem("请选择需要添加数据的表");
		btn.setVisible(false);
		Statement st = null;
		ResultSet rs = null;
		try {
			st = con.createStatement();
			String sql = "show databases";
			rs = st.executeQuery(sql);
			ArrayList<String> list = new ArrayList<String>();
			int i = 0;
			while (rs.next()) {
				list.add(rs.getString(1));
				jb.addItem(list.get(i));
				i++;
			}
		} catch (SQLException e) {
			log.debug(e);
//			e.printStackTrace();
		} finally {
			initsql.close(st, rs);
		}
		jb.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (jb.getSelectedItem().equals("请选择要操作的库")) {
					System.out.println("请选择要操作的库");
				} else {
					Statement st = null;
					ResultSet rs = null;
					btn.setVisible(true);
					jb2.removeAllItems();
					try {
						st = con.createStatement();
						String sql = "use " + jb.getSelectedItem();
						st.executeUpdate(sql);
						String sql1 = "show tables";
						rs = st.executeQuery(sql1);
						jb2.addItem("请选择需要添加数据的表");
						JButton btn2 = new JButton("确认");
						while (rs.next()) {
							jb2.addItem(rs.getString(1));
						}
						btn.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent e) {
								if (jb2.getSelectedItem().equals("请选择需要添加数据的表")) {
									System.out.println("请选择需要添加数据的表");
								} else {
									Statement st = null;
									ResultSet rs = null;
									JDialog jd3 = new JDialog();
									jd3.setTitle("以下为可以添加字段的字段名");
									jd3.setSize(1050, 800);
									jd3.setLocation(500, 500);
									jd3.setLayout(new FlowLayout());

									try {
										st = con.createStatement();
										String sql2 = "select COLUMN_NAME, column_comment from INFORMATION_SCHEMA.Columns where table_name='"
												+ jb2.getSelectedItem() + "'";
										rs = st.executeQuery(sql2);
										ArrayList<String> list2 = new ArrayList<String>();
										// 得到所有字段名，放入list2
										while (rs.next()) {
											list2.add(rs.getString(1));
										}
										JButton addbtn = new JButton("查询");
										JLabel inputTips = new JLabel("请先输入字段名再直接输入条件，用英文逗号隔开(如果输入的要求是中文请加上英文单引号)");
										JTextField addField = new JTextField(40);

										// 定义JTable的对象
										Vector rowData, columnNames;
										// 定义一行数据的对象
										Vector<String> line1;
										line1 = new Vector<String>();
										columnNames = new Vector<String>();
										rowData = new Vector();
										for (int i = 0; i < list2.size(); i++) {
											columnNames.add(list2.get(i));
										}
										String sql3 = "select*from " + jb2.getSelectedItem();// 得到所有字段的数据
										rs = st.executeQuery(sql3);
										// 所有字段的数据
										ArrayList<String> list3 = new ArrayList<String>();
										while (rs.next()) {
											for (int i = 1; i <= list2.size(); i++) {
												list3.add(rs.getString(i));
											}
										}
										int leng = 0;
										for (int j = 0; j < list3.size() / list2.size(); j++) {
											line1 = new Vector();
											for (int i = 0; i < list2.size(); i++) {
												line1.add(list3.get(leng++));
											}
											rowData.add(line1);
										}

										JTable jTable = new JTable(rowData, columnNames);
										jTable.setEnabled(false);
										addbtn.addActionListener(new ActionListener() {
											public void actionPerformed(ActionEvent e) {
												// 得到输入的结果
												String[] str = addField.getText().split(",");
												Statement st = null;
												ResultSet rs = null;
												ArrayList<String> dataNamesList = new ArrayList<String>();

												JDialog jd4 = new JDialog();
												jd4.setTitle("结果");
												jd4.setSize(1050, 800);
												jd4.setLocation(500, 500);
												jd4.setLayout(new FlowLayout());
												try {
													st = con.createStatement();
													String sql4 = "select COLUMN_NAME, column_comment from INFORMATION_SCHEMA.Columns where table_name='"
															+ jb2.getSelectedItem() + "'";
													rs = st.executeQuery(sql4);
													while (rs.next()) {
														dataNamesList.add(rs.getString(1));
													}
													// 定义JTable的对象
													Vector rowData, columnNames;
													// 定义一行数据的对象
													Vector<String> line1;
													line1 = new Vector<String>();
													columnNames = new Vector<String>();
													rowData = new Vector();
													// 写入标题
													for (int i = 0; i < dataNamesList.size(); i++) {
														columnNames.add(dataNamesList.get(i));
													}

													String sql5 = "select*from " + jb2.getSelectedItem() + " where "
															+ str[0] + "=" + str[1];
													st = con.createStatement();
													rs = st.executeQuery(sql5);

													ArrayList<String> dataList = new ArrayList<String>();
													while (rs.next()) {
														for (int i = 1; i <= dataNamesList.size(); i++) {
															dataList.add(rs.getString(i));
														}
													}
													int leng = 0;
													for (int j = 0; j < dataList.size() / dataNamesList.size(); j++) {
														line1 = new Vector();
														for (int i = 0; i < dataNamesList.size(); i++) {
															line1.add(dataList.get(leng++));
														}
														rowData.add(line1);
													}

													JTable jTable = new JTable(rowData, columnNames);
													jTable.setEnabled(false);
													JScrollPane jScrollPane = new JScrollPane(jTable);
													jd4.add(jScrollPane);
												} catch (SQLException e1) {
													log.debug(e1);
//													e1.printStackTrace();
												} finally {
													initsql.close(st, rs);
												}

												jd4.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
												jd4.setModal(true);
												jd4.setVisible(true);
											}
										});
										JScrollPane jScrollPane = new JScrollPane(jTable);
										jd3.add(jScrollPane);
										jd3.add(addField);
										jd3.add(addbtn);
										jd3.add(inputTips);
									} catch (SQLException e1) {
										log.debug(e1);
//										e1.printStackTrace();
									} finally {
										initsql.close(st, rs);
									}
									jd3.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
									jd3.setModal(true);
									jd3.setVisible(true);
								}
							}
						});
					} catch (SQLException e1) {
						log.debug(e1);
//						e1.printStackTrace();
					} finally {
						initsql.close(st, rs);
					}

				}
			}
		});
		jd.add(jb);
		jd.add(jb2);
		jd.add(btn);
		jd.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		jd.setModal(true);
		jd.setVisible(true);
	}

//修改表名
	public void AB(Connection con) {
		JDialog jd = new JDialog(this, "修改表名");
		jd.setSize(1050, 800);
		jd.setLocation(500, 500);
		jd.setLayout(new FlowLayout());
		JComboBox<String> jb = new JComboBox<String>();
		JComboBox<String> jb2 = new JComboBox<String>();
		JButton btn = new JButton("确认修改");
		JLabel Tips = new JLabel("请选择库和表，然后输入新表名");
		JTextField newFieldname = new JTextField(20);

		jb.addItem("请选择要操作的库");
		jb2.addItem("请选择需要修改的表");
		btn.setVisible(false);
		Statement st = null;
		ResultSet rs = null;
		try {
			st = con.createStatement();
			String sql = "show databases";
			rs = st.executeQuery(sql);
			ArrayList<String> list = new ArrayList<String>();
			int i = 0;
			while (rs.next()) {
				list.add(rs.getString(1));
				jb.addItem(list.get(i));
				i++;
			}
		} catch (SQLException e) {
			log.debug(e);
//			e.printStackTrace();
		} finally {
			initsql.close(st, rs);
		}
		jb.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (jb.getSelectedItem().equals("请选择要操作的库")) {
					System.out.println("请选择要操作的库");
				} else {
					Statement st = null;
					ResultSet rs = null;
					btn.setVisible(true);
					jb2.removeAllItems();
					try {
						st = con.createStatement();
						String sql = "use " + jb.getSelectedItem();
						st.executeUpdate(sql);
						String sql1 = "show tables";
						rs = st.executeQuery(sql1);
						jb2.addItem("请选择需要修改的表");
						JButton btn2 = new JButton("确认");
						while (rs.next()) {
							jb2.addItem(rs.getString(1));
						}
						btn.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent e) {
								if (jb2.getSelectedItem().equals("请选择需要修改的表")) {
									System.out.println("请选择需要修改的表");
								} else {
									Statement st = null;
									try {
										String sqlNewField = "alter table " + jb2.getSelectedItem() + " rename "
												+ newFieldname.getText();
										st = con.createStatement();
										int rsult = st.executeUpdate(sqlNewField);
										newFieldname.setText("");
										if (rsult == 0) {
											System.out.println("修改成功");
										} else {
											System.out.println("修改失败");
										}
									} catch (SQLException e1) {
										log.debug(e1);
//										e1.printStackTrace();
									} finally {
										initsql.close(st);
									}
								}
							}
						});
					} catch (SQLException e1) {
						log.debug(e1);
//						e1.printStackTrace();
					} finally {
						initsql.close(st, rs);
					}

				}
			}
		});
		jd.add(Tips);
		jd.add(jb);
		jd.add(jb2);
		jd.add(newFieldname);
		jd.add(btn);
		jd.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		jd.setModal(true);
		jd.setVisible(true);
	}

//修改字段名
	public void AFN(Connection con) {
		JDialog jd = new JDialog(this, "修改字段名");
		jd.setSize(1050, 800);
		jd.setLocation(500, 500);
		jd.setLayout(new FlowLayout());
		JComboBox<String> jb = new JComboBox<String>();
		JComboBox<String> jb2 = new JComboBox<String>();
		JComboBox<String> jb3 = new JComboBox<String>();
		JComboBox<String> jb4 = new JComboBox<String>();
		JComboBox<String> jb5 = new JComboBox<String>();
		JButton btn = new JButton("确认修改");
		JLabel Tips = new JLabel("请选择库和表以及你想要修改的字段，然后输入新字段名，再选择新字段的类型");
		JTextField newFieldname = new JTextField(20);

		jb.addItem("请选择要操作的库");
		jb2.addItem("请选择需要操作的表");
		jb3.addItem("请选择需要操作的字段");
		jb4.addItem("请选择字段修改后的类型");
		jb4.addItem("整型");
		jb4.addItem("浮点");
		jb4.addItem("串型");
		jb4.addItem("日期");
		jb4.addItem("二进制");
		jb5.addItem("------------");
		Statement st = null;
		ResultSet rs = null;
		try {
			st = con.createStatement();
			String sql = "show databases";
			rs = st.executeQuery(sql);
			ArrayList<String> list = new ArrayList<String>();
			int i = 0;
			while (rs.next()) {
				list.add(rs.getString(1));
				jb.addItem(list.get(i));
				i++;
			}
		} catch (SQLException e) {
			log.debug(e);
//			e.printStackTrace();
		} finally {
			initsql.close(st, rs);
		}
		jb4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String s = (String) jb4.getSelectedItem();
				switch (s) {
				case "整型":
					jb5.removeAllItems();
					jb5.addItem("byte");
					jb5.addItem("smallint");
					jb5.addItem("mediumint");
					jb5.addItem("int");
					jb5.addItem("bigint");
					break;
				case "浮点":
					jb5.removeAllItems();
					jb5.addItem("float");
					jb5.addItem("double");
					break;
				case "串型":
					jb5.removeAllItems();
					jb5.addItem("char");
					jb5.addItem("varchar");
					break;
				case "日期":
					jb5.removeAllItems();
					jb5.addItem("date");
					jb5.addItem("time");
					jb5.addItem("datetime");
					break;
				case "二进制":
					jb5.removeAllItems();
					jb5.addItem("binary");
					jb5.addItem("varbinary");
					break;
				default:
					jb5.addItem("------------");
					break;
				}
			}
		});
		jb.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (jb.getSelectedItem().equals("请选择要操作的库")) {
					System.out.println("请选择要操作的库");
				} else {
					Statement st = null;
					ResultSet rs = null;
					jb2.removeAllItems();
					try {
						st = con.createStatement();
						String sql = "use " + jb.getSelectedItem();
						st.executeUpdate(sql);
						String sql1 = "show tables";
						rs = st.executeQuery(sql1);
						jb2.addItem("请选择需要修改的表");
						while (rs.next()) {
							jb2.addItem(rs.getString(1));
						}
						jb2.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent e) {
								if (jb2.getSelectedItem().equals("请选择需要操作的表")) {
									System.out.println("请选择需要操作的表");
								} else {
									Statement st = null;
									ResultSet rs = null;
									try {
										String sqlShowField = "select COLUMN_NAME, column_comment from INFORMATION_SCHEMA.Columns where table_name='"
												+ jb2.getSelectedItem() + "'";
										st = con.createStatement();
										rs = st.executeQuery(sqlShowField);
										while (rs.next()) {
											jb3.addItem(rs.getString(1));
										}
										btn.addActionListener(new ActionListener() {
											public void actionPerformed(ActionEvent e) {
												if (jb3.getSelectedItem().equals("请选择需要操作的字段")) {
													System.out.println("请选择需要操作的字段");
												} else {
													Statement st = null;
													String sqlAlterFieldName = "alter table " + jb2.getSelectedItem()
															+ " change " + jb3.getSelectedItem() + " "
															+ newFieldname.getText() + " " + jb5.getSelectedItem();
													try {
														st = con.createStatement();
														st.executeUpdate(sqlAlterFieldName);
														System.out.println("修改成功");
													} catch (SQLException e1) {
														log.debug(e1);
//														e1.printStackTrace();
													} finally {
														initsql.close(st);
													}
												}
											}
										});
									} catch (SQLException e1) {
										log.debug(e1);
//										e1.printStackTrace();
									} finally {
										initsql.close(st, rs);
									}
								}
							}
						});
					} catch (SQLException e1) {
						log.debug(e1);
//						e1.printStackTrace();
					} finally {
						initsql.close(st, rs);
					}

				}
			}
		});
		jd.add(Tips);
		jd.add(jb);
		jd.add(jb2);
		jd.add(jb3);
		jd.add(jb4);
		jd.add(jb5);
		jd.add(newFieldname);
		jd.add(btn);
		jd.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		jd.setModal(true);
		jd.setVisible(true);
	}

//修改字段类型
	public void AFT(Connection con) {
		JDialog jd = new JDialog(this, "修改字段类型");
		jd.setSize(1050, 800);
		jd.setLocation(500, 500);
		jd.setLayout(new FlowLayout());
		JComboBox<String> jb = new JComboBox<String>();
		JComboBox<String> jb2 = new JComboBox<String>();
		JComboBox<String> jb3 = new JComboBox<String>();
		JComboBox<String> jb4 = new JComboBox<String>();
		JComboBox<String> jb5 = new JComboBox<String>();
		JButton btn = new JButton("确认修改");
		JLabel Tips = new JLabel("请选择库和表以及你想要修改的字段，再选择新的类型");

		jb.addItem("请选择要操作的库");
		jb2.addItem("请选择需要操作的表");
		jb3.addItem("请选择需要操作的字段");
		jb4.addItem("请选择字段修改后的类型");
		jb4.addItem("整型");
		jb4.addItem("浮点");
		jb4.addItem("串型");
		jb4.addItem("日期");
		jb4.addItem("二进制");
		jb5.addItem("------------");
		Statement st = null;
		ResultSet rs = null;
		try {
			st = con.createStatement();
			String sql = "show databases";
			rs = st.executeQuery(sql);
			ArrayList<String> list = new ArrayList<String>();
			int i = 0;
			while (rs.next()) {
				list.add(rs.getString(1));
				jb.addItem(list.get(i));
				i++;
			}
		} catch (SQLException e) {
			log.debug(e);
//			e.printStackTrace();
		} finally {
			initsql.close(st, rs);
		}
		jb4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String s = (String) jb4.getSelectedItem();
				switch (s) {
				case "整型":
					jb5.removeAllItems();
					jb5.addItem("byte");
					jb5.addItem("smallint");
					jb5.addItem("mediumint");
					jb5.addItem("int");
					jb5.addItem("bigint");
					break;
				case "浮点":
					jb5.removeAllItems();
					jb5.addItem("float");
					jb5.addItem("double");
					break;
				case "串型":
					jb5.removeAllItems();
					jb5.addItem("char");
					jb5.addItem("varchar(100)");
					break;
				case "日期":
					jb5.removeAllItems();
					jb5.addItem("date");
					jb5.addItem("time");
					jb5.addItem("datetime");
					break;
				case "二进制":
					jb5.removeAllItems();
					jb5.addItem("binary");
					jb5.addItem("varbinary");
					break;
				default:
					jb5.addItem("------------");
					break;
				}
			}
		});
		jb.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (jb.getSelectedItem().equals("请选择要操作的库")) {
					System.out.println("请选择要操作的库");
				} else {
					Statement st = null;
					ResultSet rs = null;
					jb2.removeAllItems();
					try {
						st = con.createStatement();
						String sql = "use " + jb.getSelectedItem();
						st.executeUpdate(sql);
						String sql1 = "show tables";
						rs = st.executeQuery(sql1);
						jb2.addItem("请选择需要修改的表");
						while (rs.next()) {
							jb2.addItem(rs.getString(1));
						}
						jb2.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent e) {
								if (jb2.getSelectedItem().equals("请选择需要操作的表")) {
									System.out.println("请选择需要操作的表");
								} else {
									Statement st = null;
									ResultSet rs = null;
									try {
										String sqlShowField = "select COLUMN_NAME, column_comment from INFORMATION_SCHEMA.Columns where table_name='"
												+ jb2.getSelectedItem() + "'";
										st = con.createStatement();
										rs = st.executeQuery(sqlShowField);
										while (rs.next()) {
											jb3.addItem(rs.getString(1));
										}
										btn.addActionListener(new ActionListener() {
											public void actionPerformed(ActionEvent e) {
												if (jb3.getSelectedItem().equals("请选择需要操作的字段")) {
													System.out.println("请选择需要操作的字段");
												} else {
													Statement st = null;
													String sqlAlterFieldName = "alter table " + jb2.getSelectedItem()
															+ " change " + jb3.getSelectedItem() + " "
															+ jb3.getSelectedItem() + " " + jb5.getSelectedItem();
													try {
														st = con.createStatement();
														st.executeUpdate(sqlAlterFieldName);
														System.out.println("修改成功");
													} catch (SQLException e1) {
														log.debug(e1);
//														e1.printStackTrace();
													} finally {
														initsql.close(st);
													}
												}
											}
										});
									} catch (SQLException e1) {
										log.debug(e1);
//										e1.printStackTrace();
									} finally {
										initsql.close(st, rs);
									}
								}
							}
						});
					} catch (SQLException e1) {
						log.debug(e1);
//						e1.printStackTrace();
					} finally {
						initsql.close(st, rs);
					}

				}
			}
		});
		jd.add(Tips);
		jd.add(jb);
		jd.add(jb2);
		jd.add(jb3);
		jd.add(jb4);
		jd.add(jb5);
		jd.add(btn);
		jd.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		jd.setModal(true);
		jd.setVisible(true);
	}

// 新增字段
	public void AF(Connection con) {
		JDialog jd = new JDialog(this, "添加字段");
		jd.setSize(1050, 800);
		jd.setLocation(500, 500);
		jd.setLayout(new FlowLayout());
		JComboBox<String> jb = new JComboBox<String>();
		JComboBox<String> jb2 = new JComboBox<String>();
		JComboBox<String> jb4 = new JComboBox<String>();
		JComboBox<String> jb5 = new JComboBox<String>();
		JButton btn = new JButton("确认");
		JLabel Tips = new JLabel("请选择库和表，然后输入新字段名，再选择新字段的类型");
		JLabel Tips2 = new JLabel("请输入新字段的名称");
		JTextField newFieldname = new JTextField(20);

		jb.addItem("请选择要操作的库");
		jb2.addItem("请选择需要操作的表");
		jb4.addItem("请选择添加字段的类型");
		jb4.addItem("整型");
		jb4.addItem("浮点");
		jb4.addItem("串型");
		jb4.addItem("日期");
		jb4.addItem("二进制");
		jb5.addItem("------------");
		Statement st = null;
		ResultSet rs = null;
		try {
			st = con.createStatement();
			String sql = "show databases";
			rs = st.executeQuery(sql);
			ArrayList<String> list = new ArrayList<String>();
			int i = 0;
			while (rs.next()) {
				list.add(rs.getString(1));
				jb.addItem(list.get(i));
				i++;
			}
		} catch (SQLException e) {
			log.debug(e);
//				e.printStackTrace();
		} finally {
			initsql.close(st, rs);
		}
		jb4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String s = (String) jb4.getSelectedItem();
				switch (s) {
				case "整型":
					jb5.removeAllItems();
					jb5.addItem("byte");
					jb5.addItem("smallint");
					jb5.addItem("mediumint");
					jb5.addItem("int");
					jb5.addItem("bigint");
					break;
				case "浮点":
					jb5.removeAllItems();
					jb5.addItem("float");
					jb5.addItem("double");
					break;
				case "串型":
					jb5.removeAllItems();
					jb5.addItem("char");
					jb5.addItem("varchar(100)");
					break;
				case "日期":
					jb5.removeAllItems();
					jb5.addItem("date");
					jb5.addItem("time");
					jb5.addItem("datetime");
					break;
				case "二进制":
					jb5.removeAllItems();
					jb5.addItem("binary");
					jb5.addItem("varbinary");
					break;
				default:
					jb5.addItem("------------");
					break;
				}
			}
		});
		jb.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (jb.getSelectedItem().equals("请选择要操作的库")) {
					System.out.println("请选择要操作的库");
				} else {
					Statement st = null;
					ResultSet rs = null;
					jb2.removeAllItems();
					try {
						st = con.createStatement();
						String sql = "use " + jb.getSelectedItem();
						st.executeUpdate(sql);
						String sql1 = "show tables";
						rs = st.executeQuery(sql1);
						jb2.addItem("请选择需要添加字段的表");
						while (rs.next()) {
							jb2.addItem(rs.getString(1));
						}
						jb2.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent e) {
								if (jb2.getSelectedItem().equals("请选择需要添加字段的表")) {
									System.out.println("请选择需要添加字段的表");
								} else {
									btn.addActionListener(new ActionListener() {
										public void actionPerformed(ActionEvent e) {
											Statement st = null;
											String addFieldName = "alter table " + jb2.getSelectedItem()
													+ " add column " + newFieldname.getText() + " "
													+ jb5.getSelectedItem() + " default null;";
											try {
												st = con.createStatement();
												st.executeUpdate(addFieldName);
												System.out.println("添加成功");
											} catch (SQLException e1) {
												log.debug(e1);
//															e1.printStackTrace();
											} finally {
												initsql.close(st);
											}
										}
									});
								}
							}
						});
					} catch (SQLException e1) {
						log.debug(e1);
//							e1.printStackTrace();
					} finally {
						initsql.close(st, rs);
					}
				}
			}
		});
		jd.add(Tips);
		jd.add(jb);
		jd.add(jb2);
		jd.add(jb4);
		jd.add(jb5);
		jd.add(Tips2);
		jd.add(newFieldname);
		jd.add(btn);
		jd.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		jd.setModal(true);
		jd.setVisible(true);
	}

// 删除库
	public void DK(Connection con) {
		JDialog jd = new JDialog(this, "删除库-----------注：不可逆转");
		jd.setSize(1050, 800);
		jd.setLocation(500, 500);
		jd.setLayout(new FlowLayout());
		JComboBox<String> jb = new JComboBox<String>();
		jb.addItem("选择要删除的库");
		JButton btn = new JButton("确认删除");
		Statement st = null;
		ResultSet rs = null;
		try {
			st = con.createStatement();
			String sql = "show databases";
			rs = st.executeQuery(sql);
			ArrayList<String> list = new ArrayList<String>();
			int i = 0;
			while (rs.next()) {
				list.add(rs.getString(1));
				jb.addItem(list.get(i));
				i++;
			}
		} catch (SQLException e) {
			log.debug(e);
//				e.printStackTrace();
		} finally {
			initsql.close(st, rs);
		}
		jd.add(jb);
		jd.add(btn);
		btn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Statement st = null;
				try {
					st = con.createStatement();
					String sql = "drop database " + jb.getSelectedItem();
					st.executeUpdate(sql);
					System.out.println("删除成功");
				} catch (SQLException e1) {
					System.out.println("失败");
					log.debug(e1);
//					e1.printStackTrace();
				} finally {
					initsql.close(st);
				}
			}
		});
		jd.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		jd.setModal(true);
		jd.setVisible(true);
	}

// 删除表
	public void DB(Connection con) {
		JDialog jd = new JDialog(this, "删除表-----------注：不可逆转");
		jd.setSize(1050, 800);
		jd.setLocation(500, 500);
		jd.setLayout(new FlowLayout());
		JComboBox<String> jb = new JComboBox<String>();
		JComboBox<String> jb2 = new JComboBox<String>();
		jb.addItem("请先选择库");
		jb2.addItem("请选择要删除的表");
		JButton btn = new JButton("确认删除");
		Statement st = null;
		ResultSet rs = null;
		try {
			st = con.createStatement();
			String sql = "show databases";
			rs = st.executeQuery(sql);
			ArrayList<String> list = new ArrayList<String>();
			int i = 0;
			while (rs.next()) {
				list.add(rs.getString(1));
				jb.addItem(list.get(i));
				i++;
			}
		} catch (SQLException e) {
			log.debug(e);
//					e.printStackTrace();
		} finally {
			initsql.close(st, rs);
		}
		jd.add(jb);
		jd.add(jb2);
		jd.add(btn);
		jb.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (jb.getSelectedItem().equals("请先选择库")) {
					System.out.println("请先选择库");
				} else {
					Statement st = null;
					ResultSet rs = null;
					jb2.removeAllItems();
					try {
						jb2.addItem("请选择要删除的表");
						st = con.createStatement();
						String FieldNames = "use " + jb.getSelectedItem();
						st.executeUpdate(FieldNames);
						String showAllField = "show tables";
						rs = st.executeQuery(showAllField);
						while (rs.next()) {
							jb2.addItem(rs.getString(1));
						}
						btn.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent e) {
								Statement st = null;
								try {
									st = con.createStatement();
									String delField = "drop table " + jb2.getSelectedItem();
									st.executeUpdate(delField);
									System.out.println("删除成功");
								} catch (SQLException e1) {
									System.out.println("失败");
									log.debug(e1);
//										e1.printStackTrace();
								} finally {
									initsql.close(st);
								}
							}
						});
					} catch (SQLException e1) {
						log.debug(e1);
//							e1.printStackTrace();
					} finally {
						initsql.close(st, rs);
					}
				}

			}
		});

		jd.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		jd.setModal(true);
		jd.setVisible(true);
	}

// 删除字段
	public void DF(Connection con) {
		JDialog jd = new JDialog(this, "删除字段-----------注：不可逆转");
		jd.setSize(1050, 800);
		jd.setLocation(500, 500);
		jd.setLayout(new FlowLayout());
		JComboBox<String> jb = new JComboBox<String>();
		JComboBox<String> jb2 = new JComboBox<String>();
		JComboBox<String> jb3 = new JComboBox<String>();
		jb.addItem("请选择库");
		jb2.addItem("请选择表");
		jb3.addItem("请选择要删除的字段");
		JButton btn = new JButton("确认删除");
		Statement st = null;
		ResultSet rs = null;
		try {
			st = con.createStatement();
			String sql = "show databases";
			rs = st.executeQuery(sql);
			ArrayList<String> list = new ArrayList<String>();
			int i = 0;
			while (rs.next()) {
				list.add(rs.getString(1));
				jb.addItem(list.get(i));
				i++;
			}
		} catch (SQLException e) {
			log.debug(e);
//						e.printStackTrace();
		} finally {
			initsql.close(st, rs);
		}
		jd.add(jb);
		jd.add(jb2);
		jd.add(jb3);
		jd.add(btn);
		jb.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (jb.getSelectedItem().equals("请选择库")) {
					System.out.println("请选择库");
				} else {
					Statement st = null;
					ResultSet rs = null;
					jb2.removeAllItems();
					try {
						jb2.addItem("请选择要删除的表");
						st = con.createStatement();
						String FieldNames = "use " + jb.getSelectedItem();
						st.executeUpdate(FieldNames);
						String showAllField = "show tables";
						rs = st.executeQuery(showAllField);
						while (rs.next()) {
							jb2.addItem(rs.getString(1));
						}
						jb2.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent e) {
								Statement st = null;
								ResultSet rs = null;
								String sqlShowField = "select COLUMN_NAME, column_comment from INFORMATION_SCHEMA.Columns where table_name='"
										+ jb2.getSelectedItem() + "'";
								jb3.removeAllItems();
								jb3.addItem("请选择要删除的字段");
								try {
									st = con.createStatement();
									rs = st.executeQuery(sqlShowField);
									while (rs.next()) {
										jb3.addItem(rs.getString(1));
									}
									btn.addActionListener(new ActionListener() {
										public void actionPerformed(ActionEvent e) {
											if (jb3.getSelectedItem().equals("请选择要删除的字段")) {
												System.out.println("请选择要删除的字段");
											} else {
												Statement st = null;
												try {
													st = con.createStatement();
													String delField = "alter table " + jb2.getSelectedItem()
															+ " drop column " + jb3.getSelectedItem();
													st.executeUpdate(delField);
													System.out.println("删除成功");
												} catch (SQLException e1) {
													System.out.println("失败");
													log.debug(e1);
//															e1.printStackTrace();
												} finally {
													initsql.close(st);
												}
											}

										}
									});
								} catch (SQLException e1) {
									log.debug(e1);
//									e1.printStackTrace();
								} finally {
									initsql.close(st, rs);
								}
							}
						});

					} catch (SQLException e1) {
						log.debug(e1);
//								e1.printStackTrace();
					} finally {
						initsql.close(st, rs);
					}
				}

			}
		});

		jd.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		jd.setModal(true);
		jd.setVisible(true);
	}
}