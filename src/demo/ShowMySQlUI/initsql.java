package ShowMySQlUI;

import java.sql.*;

public class initsql {

	private static final String url = "jdbc:mysql://localhost:3306";
	private static final String user = "root";
	private static final String pwd = "8520";
	static Connection con = null;

	public static Connection getcon() {
		// 加载驱动
		try {
			Class.forName("com.mysql.jdbc.Driver");
			System.out.println("加载驱动成功");
		} catch (ClassNotFoundException e) {
			System.out.println("加载驱动失败");
			e.printStackTrace();
		}
		// 链接数据库
		try {
			con = DriverManager.getConnection(url, user, pwd);
			System.out.println("链接成功");
			return con;
		} catch (SQLException e) {
			System.out.println("连接失败");
			e.printStackTrace();
		}
		return con;
	}

	public static void close(Connection connection, Statement statement, ResultSet resultSet) {
		try {
			resultSet.close();
		} catch (SQLException e) {
		}
		try {
			statement.close();
		} catch (SQLException e) {
		}
		try {
			connection.close();
		} catch (SQLException e) {
		}
	}

	public static void close(Connection connection, Statement Statement) {
		try {
			Statement.close();
		} catch (SQLException e) {
		}
		try {
			connection.close();
		} catch (SQLException e) {
		}
	}

	public static void close(Statement Statement, ResultSet resultSet) {
		try {
			resultSet.close();
		} catch (SQLException e) {
		}
		try {
			Statement.close();
		} catch (SQLException e) {
		}
	}

	public static void close(Connection connection) {
		try {
			connection.close();
		} catch (SQLException e) {
		}
	}

	public static void close(Statement Statement) {
		try {
			Statement.close();
		} catch (SQLException e) {
		}
	}

	public static void close(Statement Statement, ResultSet resultSet, PreparedStatement preparedStatement) {
		try {
			preparedStatement.close();
		} catch (SQLException e) {
		}
		try {
			resultSet.close();
		} catch (SQLException e) {
		}
		try {
			Statement.close();
		} catch (SQLException e) {
		}
	}
}
